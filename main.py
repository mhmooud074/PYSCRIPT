import re

tequal = r"\=|\s+\=|\=\s+|\s+\=\s+"
tdequal = r"\=|\s+\=\=|\=\=\s+|\s+\=\=\s+"
tpequal = r"\+\=|\s+\+\=|\+\=\s+|\s+\+\=\s+"
tmiequal = r"\-\=|\s+\-\=|\-\=\s+|\s+\-\=\s+"
tmlequal = r"\*\=|\s+\*\=|\*\=\s+|\s+\*\=\s+"
tdequal = r"\/\=|\s+\/\=|\/\=\s+|\s+\/\=\s+"
tvar = r"var (\w+)({}|{}|{}|{})(\S.*)".format(tequal, tpequal, tmiequal, tmlequal, tdequal)
tint = r"\d+"
tfloat = r"\d+\.\d+"
tnum = r"{}|{}".format(tint, tfloat)
tscout = r"\'"
tdcout = r"\""
tchar = r"{0}(\w){0}|{1}(\w){1}".format(tscout, tdcout)
tstr = r"{0}(\w+){0}|{1}(\w+){1}".format(tscout, tdcout)
tnstr = r"{0}{0}|{1}{1}".format(tscout, tdcout)
tarray = r"\[(.*)\]"
tnline = r"\n"
ttab = r"\t"
tspace = " "
tnone = "none"
tunknown = "unknown"

class tToken:
	def __init__(self, x):
		self.x = x
		self.type = None
		self.advance()

	def advance(self):
		revar = re.search(tvar, self.x)
		if revar:self.type = f'\nvar  =>({revar.group(1)})\nequal=>({revar.group(2)})\nvalue=>({revar.group(3)})'
		elif re.search(tint, self.x):self.type = 'int'
		elif re.search(tfloat, self.x):self.type = 'float'
		elif re.search(tnum, self.x):self.type = 'num'
		elif re.search(tchar, self.x):self.type = 'char'
		elif re.search(tstr, self.x):self.type = 'str'
		elif re.search(tnstr, self.x):self.type = 'nstr'
		elif re.search(tscout, self.x):self.type = 'scout'
		elif re.search(tdcout, self.x):self.type = 'dcout'
		elif re.search(tarray, self.x):self.type = 'array'
		elif re.search(tnline, self.x):self.type = 'nline'
		elif re.search(ttab, self.x):self.type = 'tab'
		elif re.search(tspace, self.x):self.type = 'space'
		elif re.search(tnone, self.x):self.type = 'none'
		else:self.type = tunknown
		print(self.x, self.type)

while True:
	tToken(input(">>> "))